Currency Converter App
--------------------------------

This app is a basic conversion app used to find the conversion rates among various currencies.
It gives you a list of supported currencies that you can choose from and convert to.
You can also swap between the chosen currencies.


Architecture:
The architecture of this app is based on MVVM pattern, used dagger-Hilt for dependency injection,
coroutines for async calls. Used Retrofit2 HTTP client for making API calls.

API Details:
Consumed https://exchangerate.host/ API for doing the operations needed for the project.

Installation:
Unzip the project and import it into the Android studio. After importing Sync and Build the project.
Make sure that all the dependencies are correctly imported.

The app supports Android devices with OS Android 6.0(Marshmallow) and above

Observations:
Few things to take note of -
* At nights the service will either be down or will have high latencies

* When you enter an amount like 0.00000001 (with zero's up to more than five decimal places)
even though the format is correct the service returns null value as the result, after the
conversion when exchange rate is low. I have handled this by throwing an Invalid amount error.

* For the currency Cape Verdean Escudo - we get empty symbol from the currency instance.
So I have handled it by setting the symbol explicitly.

* I took care of rounding off the decimal places, in result from the UI side.