package com.example.currencyconverter.di

import com.example.currencyconverter.common.utils.DispatcherProvider
import com.example.currencyconverter.main.repository.FakeMainRepository
import com.example.currencyconverter.main.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [AppModule::class])
object TestAppModule {

    @Provides
    fun provideTestDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }


    @Provides
    fun provideTestMainRepository(): MainRepository = FakeMainRepository()
}