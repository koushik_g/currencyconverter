package com.example.currencyconverter.main.repository

import com.example.currencyconverter.common.utils.Resource
import com.example.currencyconverter.data.models.ConvertedCurrencyResponse
import com.example.currencyconverter.data.models.CurrencyDetails
import com.example.currencyconverter.data.models.Info
import com.example.currencyconverter.data.models.Query
import java.math.BigDecimal

class FakeMainRepository: MainRepository {

    private var shouldReturnNetworkError = false

    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    override suspend fun getCurrencies(): Resource<ArrayList<CurrencyDetails>> {
        return if (shouldReturnNetworkError) {
            Resource.Error("Error")
        } else {
            Resource.Success(arrayListOf(CurrencyDetails("AUD", "")))
        }
    }

    override suspend fun getConvertedAmount(
        from: String,
        to: String,
        amount: String
    ): Resource<ConvertedCurrencyResponse> {
        return if (shouldReturnNetworkError) {
            Resource.Error("Error")
        } else {
            Resource.Success(ConvertedCurrencyResponse("08-30-2021", true, Info(72.00), Query(BigDecimal(33), "AUD", "USD"),BigDecimal(40), true ))
        }
    }
}