package com.example.currencyconverter.main.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.SmallTest
import com.example.currencyconverter.common.Constants
import com.example.currencyconverter.common.utils.DispatcherProvider
import com.example.currencyconverter.common.utils.NetworkHelper
import com.example.currencyconverter.data.models.CurrencyDetails
import com.example.currencyconverter.getOrAwaitValueTest
import com.example.currencyconverter.main.repository.FakeMainRepository
import com.example.currencyconverter.main.repository.MainRepository
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal
import javax.inject.Inject

@SmallTest
@HiltAndroidTest
class MainViewModelTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val context: Context = ApplicationProvider.getApplicationContext<Context>()
    @Inject
    lateinit var dispatchers: DispatcherProvider
    @Inject
    lateinit var repository: MainRepository

    private lateinit var viewModel: MainViewModel
    lateinit var networkHelper: NetworkHelper

    @Before
    fun setup() {
        hiltRule.inject()
        networkHelper = NetworkHelper(context)
        viewModel = MainViewModel(repository, dispatchers, networkHelper)
    }

    @Test
    fun conversionCallWithEmptyAmount_returnsError() {
        viewModel.convert(context, "", "AUD", "USD")
        val value = viewModel.conversion.getOrAwaitValueTest()
        assert(value is MainViewModel.CurrencyEvent.Failure)
    }

    @Test
    fun conversionCallWithFromCurrencyNotSelected_returnsError() {
        viewModel.convert(context, "22", "", "USD")
        val value = viewModel.conversion.getOrAwaitValueTest()
        assert(value is MainViewModel.CurrencyEvent.Failure)
    }

    @Test
    fun conversionCallWithToCurrencyNotSelected_returnsError() {
        viewModel.convert(context, "22", "AUD", "")
        val value = viewModel.conversion.getOrAwaitValueTest()
        assert(value is MainViewModel.CurrencyEvent.Failure)
    }

    @Test
    fun conversionCallWithInvalidAmount_returnsError() {
        viewModel.convert(context, "00000000", "AUD", "")
        val value = viewModel.conversion.getOrAwaitValueTest()
        assert(value is MainViewModel.CurrencyEvent.Failure)
    }

    @Test
    fun conversionCallWithValidInput_returnsSuccess() {
        viewModel.convert(context, "34", "USD", "INR")
        val value = viewModel.conversion.getOrAwaitValueTest()
        assertThat(value is MainViewModel.CurrencyEvent.Success)
    }

    @Test
    fun currencyListCallWithNetworkError_returnsError() {
        val repo = repository as FakeMainRepository
        repo.setShouldReturnNetworkError(true)
        viewModel.loadCurrencyList(context)
        val value = viewModel.currencyList.getOrAwaitValueTest()
        assertThat(value is MainViewModel.CurrencyListEvent.Failure)
    }

    @Test
    fun swapSourceAmountSuccessTest() {
        viewModel.handleSwap(".12", "2.3e+12")
        val value = viewModel.processedValues.getOrAwaitValueTest()
        assert(value.first == "0.12")
    }

    @Test
    fun swapSourceAmountFailureTest() {
        viewModel.handleSwap(".", "2.3e+12")
        val value = viewModel.processedValues.getOrAwaitValueTest()
        assert(value.first != "0.12")
    }

    @Test
    fun swapDestAmountSuccessTest() {
        viewModel.handleSwap(".12", "2.3e+12")
        val value = viewModel.processedValues.getOrAwaitValueTest()
        assert(value.second == "2.3e+12".toBigDecimal().setScale(Constants.VALUE_TWO, BigDecimal.ROUND_HALF_UP)
            .toString())
    }

    @Test
    fun getSymbolSuccessTest() {
        viewModel.getSymbol(true, CurrencyDetails("USD", "United States"))
        val value = viewModel.srcSymbol.getOrAwaitValueTest()
        assert(value == "$")
    }

    @Test
    fun getSymbolFailureTest() {
        viewModel.getSymbol(true, CurrencyDetails("INR", "United States"))
        val value = viewModel.srcSymbol.getOrAwaitValueTest()
        assert(value != "$")
    }
}