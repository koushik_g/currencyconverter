package com.example.currencyconverter.main.views

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.MediumTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
@MediumTest
@HiltAndroidTest
class MainActivityTest {

    @get:Rule(order = 0)
    var hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val activityScenario = ActivityScenarioRule(MainActivity::class.java)

    //val context = ApplicationProvider.getApplicationContext<Context>()

    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun checkActivityCreatedTest() {
        activityScenario.scenario
    }

}