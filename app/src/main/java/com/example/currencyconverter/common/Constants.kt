package com.example.currencyconverter.common

object Constants {
    const val BASE_URL = "https://api.exchangerate.host/"
    const val VALUE_TWO = 2
    const val ZERO_DEGREES = 0f
    const val ONE_EIGHTY_DEGREES = 180f
    const val THREE_HUNDRED: Long = 300
    const val HALF_FLOAT = 0.5F
    const val EMPTY = ""
    const val POINT = "."
    const val SERVICE_ERROR = "Service call Failed!"
    const val PARSING_ERROR = "Response Parsing failed!"
    const val SYMBOL_KEY = "symbols"
    const val CODE_KEY = "code"
    const val DESCRIPTION_KEY = "description"
    const val STRING_ZERO = "0"
    const val HINT_TEXT = "AAA"
    const val CVE = "CVE"
    const val DOLLAR = "$"
    const val HINT_DESCRIPTION_TEXT = "Choose the currency"
    const val EXPONENT_TEXT = "e"
    const val SOURCE_POSITION = "src_position"
    const val DEST_POSITION = "dest_position"
    const val NEGATIVE_ONE = -1
}