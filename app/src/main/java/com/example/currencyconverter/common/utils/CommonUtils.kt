package com.example.currencyconverter.common.utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.example.currencyconverter.common.Constants
import java.text.NumberFormat
import java.util.*


object CommonUtils {

    fun rotateAnimation(): RotateAnimation {
        return RotateAnimation(
            Constants.ZERO_DEGREES, Constants.ONE_EIGHTY_DEGREES,
            Animation.RELATIVE_TO_SELF, Constants.HALF_FLOAT,
            Animation.RELATIVE_TO_SELF, Constants.HALF_FLOAT
        ).apply {
            duration = Constants.THREE_HUNDRED
            interpolator = LinearInterpolator()
        }
    }

    fun getCurrencyLocaleMap(): Map<String, Locale> {
        val map: MutableMap<String, Locale> = HashMap()
        for (locale in Locale.getAvailableLocales()) {
            try {
                val code: String =
                    NumberFormat.getCurrencyInstance(locale).currency?.currencyCode.toString()
                map[code] = locale
            } catch (e: Exception) {
                // skip strange locale
            }
        }
        return map
    }

    fun hideKeyboard(view: View) {
        val inputMethodManager =
            view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(view: View) {
        val imm = view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun showOneButtonDialog(context: Context, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener { dialog, _ ->
                dialog.cancel()
            })
        val alert: AlertDialog = builder.create()
        alert.show()
    }
}