package com.example.currencyconverter.data.models

import java.math.BigDecimal

data class ConvertedCurrencyResponse(
    val date: String,
    val historical: Boolean,
    val info: Info,
    val query: Query,
    val result: BigDecimal,
    val success: Boolean
)