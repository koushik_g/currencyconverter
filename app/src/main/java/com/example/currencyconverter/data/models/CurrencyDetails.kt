package com.example.currencyconverter.data.models

data class CurrencyDetails(
    val code: String,
    val description: String
) {
    override fun toString(): String {
        return "CurrencyDetails(code='$code', description='$description')"
    }
}