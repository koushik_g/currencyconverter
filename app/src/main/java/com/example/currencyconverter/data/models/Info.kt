package com.example.currencyconverter.data.models

data class Info(
    val rate: Double
)