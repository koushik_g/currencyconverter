package com.example.currencyconverter.data.models

import java.math.BigDecimal

data class Query(
    val amount: BigDecimal,
    val from: String,
    val to: String
)