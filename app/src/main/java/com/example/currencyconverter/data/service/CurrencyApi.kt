package com.example.currencyconverter.data.service

import com.example.currencyconverter.data.models.ConvertedCurrencyResponse
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/symbols")
    suspend fun getCurrencies(): Response<ResponseBody>

    @GET("/convert")
    suspend fun getConvertedAmount(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("amount") amount: String
    ): Response<ConvertedCurrencyResponse>
}