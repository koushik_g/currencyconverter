package com.example.currencyconverter.main.repository

import com.example.currencyconverter.common.Constants
import com.example.currencyconverter.common.utils.Resource
import com.example.currencyconverter.data.service.CurrencyApi
import com.example.currencyconverter.data.models.ConvertedCurrencyResponse
import com.example.currencyconverter.data.models.CurrencyDetails
import org.json.JSONObject
import javax.inject.Inject

class DefaultMainRepository @Inject constructor(
    private val api: CurrencyApi
) : MainRepository {

    override suspend fun getCurrencies(): Resource<ArrayList<CurrencyDetails>> {
        return try {
            val response = api.getCurrencies()
            var responseString: String

            if (null != response && response.code() == 200) {
                responseString = response.body()?.string() ?: Constants.EMPTY
            } else {
                return Resource.Error(Constants.SERVICE_ERROR)
            }

            val currencyList = parseCurrencyListResponse(responseString)
            if (response.isSuccessful && currencyList.size > 0) {
                Resource.Success(currencyList)
            } else {
                Resource.Error(Constants.SERVICE_ERROR)
            }
        } catch (e: Exception) {
            Resource.Error(Constants.PARSING_ERROR)
        }
    }

    override suspend fun getConvertedAmount(
        from: String,
        to: String,
        amount: String
    ): Resource<ConvertedCurrencyResponse> {
        return try {
            val response = api.getConvertedAmount(from, to, amount)
            lateinit var result: ConvertedCurrencyResponse

            if (null != response && response.code() == 200) {
                result = response.body()!!
            } else {
                return Resource.Error(Constants.SERVICE_ERROR)
            }

            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(Constants.SERVICE_ERROR)
            }
        } catch (e: Exception) {
            Resource.Error(Constants.PARSING_ERROR)
        }
    }

    private fun parseCurrencyListResponse(response: String): ArrayList<CurrencyDetails> {
        val currencyList = ArrayList<CurrencyDetails>()
        //adding spinner hint at the end
        currencyList.add(CurrencyDetails(Constants.HINT_TEXT, Constants.HINT_DESCRIPTION_TEXT))
        var keyStr: String
        lateinit var obj: JSONObject

        if (response.isNotEmpty()) {
            val output = JSONObject(response)
            val symbolObject = output.getJSONObject(Constants.SYMBOL_KEY)
            val keysIterator = symbolObject.keys()
            while (keysIterator.hasNext()) {
                keyStr = keysIterator.next() as String
                obj = symbolObject.getJSONObject(keyStr)
                currencyList.add(
                    CurrencyDetails(
                        obj?.getString(Constants.CODE_KEY)!!,
                        obj?.getString(Constants.DESCRIPTION_KEY)!!
                    )
                )
            }
        }
        return currencyList
    }
}