package com.example.currencyconverter.main.repository

import com.example.currencyconverter.common.utils.Resource
import com.example.currencyconverter.data.models.ConvertedCurrencyResponse
import com.example.currencyconverter.data.models.CurrencyDetails

interface MainRepository {

    suspend fun getCurrencies(): Resource<ArrayList<CurrencyDetails>>

    suspend fun getConvertedAmount(
        from: String,
        to: String,
        amount: String
    ): Resource<ConvertedCurrencyResponse>
}