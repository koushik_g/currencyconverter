package com.example.currencyconverter.main.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.currencyconverter.R
import com.example.currencyconverter.common.Constants
import com.example.currencyconverter.common.utils.CommonUtils
import com.example.currencyconverter.common.utils.DispatcherProvider
import com.example.currencyconverter.common.utils.NetworkHelper
import com.example.currencyconverter.common.utils.Resource
import com.example.currencyconverter.data.models.CurrencyDetails
import com.example.currencyconverter.main.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository,
    private val dispatchers: DispatcherProvider,
    private val networkHelper: NetworkHelper
) : ViewModel() {

    sealed class CurrencyEvent {
        class Success(val resultText: String) : CurrencyEvent()
        class Failure(val errorText: Pair<Boolean, String>) : CurrencyEvent()
        object Loading : CurrencyEvent()
        object Empty : CurrencyEvent()
    }

    sealed class CurrencyListEvent {
        class Success(val resultList: ArrayList<CurrencyDetails>) : CurrencyListEvent()
        class Failure(val errorText: String) : CurrencyListEvent()
        object Loading : CurrencyListEvent()
        object Empty : CurrencyListEvent()
    }

    private val mCurrencyList = MutableLiveData<CurrencyListEvent>(CurrencyListEvent.Empty)
    val currencyList: LiveData<CurrencyListEvent> = mCurrencyList

    private val mConversion = MutableLiveData<CurrencyEvent>(CurrencyEvent.Empty)
    val conversion: LiveData<CurrencyEvent> = mConversion

    private val mLocaleMap = MutableLiveData<Map<String, Locale>>()

    private val mSrcSymbol = MutableLiveData<String>()
    val srcSymbol: LiveData<String> = mSrcSymbol

    private val mDestSymbol = MutableLiveData<String>()
    val destSymbol: LiveData<String> = mDestSymbol

    private val mProcessedValues = MutableLiveData<Pair<String, String>>()
    val processedValues: LiveData<Pair<String, String>> = mProcessedValues

    init {
        viewModelScope.launch(dispatchers.io) {
            getMapOnMain(CommonUtils.getCurrencyLocaleMap())
        }
    }

    private fun getMapOnMain(map: Map<String, Locale>) {
        mLocaleMap.postValue(map)
    }

    fun convert(context: Context, amount: String, fromCurrency: String, toCurrency: String) {
        //handling network failure
        if (!networkHelper.isNetworkConnected()) {
            mConversion.value =
                CurrencyEvent.Failure(true to context.getString(R.string.network_error))
            return
        }

        //handling if spinners text is empty or has hint selected
        if (fromCurrency.isEmpty() || toCurrency.isEmpty() || fromCurrency == Constants.HINT_TEXT || toCurrency == Constants.HINT_TEXT) {
            mConversion.value =
                CurrencyEvent.Failure(true to context.getString(R.string.invalid_currency_error))
            return
        }

        //handling invalid inputs in source amount field
        //val pattern = Regex(Constants.INVALID_CHAR_REGEX)
        if (amount.isEmpty() || amount == Constants.POINT || amount.toBigDecimal() <= BigDecimal.ZERO) {
            mConversion.value =
                CurrencyEvent.Failure(false to context.getString(R.string.invalid_source_amount_error))
            return
        }

        //handling if source and dest currencies are same(mitigating service call)
        if (fromCurrency == toCurrency) {
            mConversion.value = CurrencyEvent.Success(amount)
            return
        }

        viewModelScope.launch(dispatchers.io) {
            mConversion.postValue(CurrencyEvent.Loading)
            when (val conversionResponse =
                repository.getConvertedAmount(fromCurrency, toCurrency, amount)) {
                is Resource.Error -> mConversion.postValue(CurrencyEvent.Failure(true to conversionResponse.message.toString()))
                is Resource.Success -> {
                    var convertedAmount = conversionResponse.data?.result
                    if (convertedAmount == null) {
                        mConversion.postValue(CurrencyEvent.Failure(true to context.getString(R.string.invalid_source_amount_error)))
                    } else {
                        if (convertedAmount < BigDecimal.ONE ||
                            convertedAmount.toString().contains(Constants.EXPONENT_TEXT, true)
                        ) {
                            mConversion.postValue(CurrencyEvent.Success(convertedAmount.toString()))
                        } else {
                            convertedAmount = convertedAmount.setScale(
                                Constants.VALUE_TWO,
                                BigDecimal.ROUND_HALF_UP
                            )
                            mConversion.postValue(CurrencyEvent.Success(convertedAmount.toString()))
                        }
                    }
                }
            }
        }
    }

    fun loadCurrencyList(context: Context) {
        //handling network failure
        if (!networkHelper.isNetworkConnected()) {
            mCurrencyList.value =
                CurrencyListEvent.Failure(context.getString(R.string.network_error))
            return
        }

        viewModelScope.launch(dispatchers.io) {
            mCurrencyList.postValue(CurrencyListEvent.Loading)
            when (val currenciesResponse = repository.getCurrencies()) {
                is Resource.Error -> mCurrencyList.postValue(
                    CurrencyListEvent.Failure(
                        currenciesResponse.message.toString()
                    )
                )
                is Resource.Success -> {
                    mCurrencyList.postValue(CurrencyListEvent.Success(currenciesResponse.data!!))
                }
            }
        }
    }

    fun getSymbol(isSource: Boolean, item: CurrencyDetails?) {
        if (item != null) {
            val cur = Currency.getInstance(item.code)
            var symbol = cur.getSymbol(mLocaleMap.value?.get(item.code) ?: Locale.US)
            when (item.code) {
                Constants.HINT_TEXT -> symbol = Constants.EMPTY
                Constants.CVE -> symbol = Constants.DOLLAR
            }
            if (isSource) {
                mSrcSymbol.value = symbol
            } else {
                mDestSymbol.value = symbol
            }
        }
    }

    fun handleSwap(srcAmnt: String, destAmnt: String) {
        var source: String = srcAmnt
        var destination: String = destAmnt
        if (srcAmnt.isNotEmpty() && srcAmnt.startsWith(Constants.POINT) && srcAmnt.length > 1) {
            source = "0$srcAmnt"
        }
        if (destAmnt.isNotEmpty() && destAmnt.contains(Constants.EXPONENT_TEXT, true)) {
            destination =
                destAmnt.toBigDecimal().setScale(Constants.VALUE_TWO, BigDecimal.ROUND_HALF_UP)
                    .toString()
        }
        mProcessedValues.value = source to destination
    }

}