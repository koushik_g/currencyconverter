package com.example.currencyconverter.main.views

import android.content.Context
import android.graphics.Color.BLACK
import android.graphics.Color.GRAY
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.view.isVisible
import com.example.currencyconverter.R
import com.example.currencyconverter.common.Constants
import com.example.currencyconverter.data.models.CurrencyDetails

class CurrencyAdapter(context: Context, currencyList: ArrayList<CurrencyDetails>) :
    ArrayAdapter<CurrencyDetails>(context, 0, currencyList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent, false)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position, convertView, parent, true)
    }

    private fun initView(
        position: Int,
        convertView: View?,
        parent: ViewGroup,
        isDropDownView: Boolean
    ): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_spinner_item, parent, false)
        }
        val itemView = view?.findViewById<TextView>(R.id.item_txt)
        val dropDownItemView = view?.findViewById<TextView>(R.id.dropdown_item_txt)
        val item = getItem(position)
        if (isDropDownView) {
            dropDownItemView?.isVisible = true
            itemView?.isVisible = false
            if (item?.code == Constants.HINT_TEXT) {
                dropDownItemView?.text = item.description ?: Constants.EMPTY
            } else {
                dropDownItemView?.text = String.format(
                    context.getString(R.string.dropdown_text),
                    item?.code,
                    item?.description
                )
            }
            if (position == 0) dropDownItemView?.setTextColor(GRAY) else dropDownItemView?.setTextColor(
                BLACK
            )
        } else {
            dropDownItemView?.isVisible = false
            itemView?.isVisible = true
            if (item?.code == Constants.HINT_TEXT) {
                itemView?.text = item?.description ?: Constants.EMPTY
            } else {
                itemView?.text = String.format(
                    context.getString(R.string.dropdown_text),
                    item?.code,
                    item?.description
                )
            }
        }

        return view!!
    }

    override fun isEnabled(position: Int): Boolean {
        return position != 0
    }
}