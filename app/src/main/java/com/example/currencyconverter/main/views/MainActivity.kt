package com.example.currencyconverter.main.views

import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.currencyconverter.R
import com.example.currencyconverter.common.Constants
import com.example.currencyconverter.common.utils.CommonUtils
import com.example.currencyconverter.data.models.CurrencyDetails
import com.example.currencyconverter.databinding.ActivityMainBinding
import com.example.currencyconverter.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.util.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var currencyAdapter: CurrencyAdapter
    private var srcCurrencyPosition = -1
    private var destCurrencyPosition = -1
    private var isSrcDropDownClicked = false
    private var isDestDropDownClicked = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setToolBar()
        setListeners()
        setObservers()
        loadData()

        binding.destEditTxt.isEnabled = false
        setupConnectingUiState()
        viewModel.loadCurrencyList(this)
    }

    private fun setObservers() {
        viewModel.srcSymbol.observe(this, Observer {
            binding.srcCurrencySymbol.text = it
        })

        viewModel.destSymbol.observe(this, Observer {
            binding.destCurrencySymbol.text = it
        })

        viewModel.processedValues.observe(this, Observer {
            binding.apply {
                srcCurrencyDropDown.setSelection(destCurrencyPosition)
                destCurrencyDropDown.setSelection(srcCurrencyPosition)
                srcEditTxt.setText(it.second)
                destEditTxt.setText(it.first)
            }
        })
    }

    private fun loadData() {
        lifecycleScope.launchWhenStarted {
            viewModel.currencyList.observe(this@MainActivity, Observer {
                when (it) {
                    is MainViewModel.CurrencyListEvent.Success -> {
                        binding.progress.isVisible = false
                        currencyAdapter = CurrencyAdapter(this@MainActivity, it.resultList)
                        binding.srcCurrencyDropDown.adapter = currencyAdapter
                        binding.destCurrencyDropDown.adapter = currencyAdapter
                        if (srcCurrencyPosition > Constants.NEGATIVE_ONE && destCurrencyPosition > Constants.NEGATIVE_ONE) {
                            binding.srcCurrencyDropDown.setSelection(srcCurrencyPosition)
                            binding.destCurrencyDropDown.setSelection(destCurrencyPosition)
                        }
                        setupIdleUiState()
                    }
                    is MainViewModel.CurrencyListEvent.Failure -> {
                        binding.progress.isVisible = false
                        CommonUtils.showOneButtonDialog(this@MainActivity, it.errorText)
                        setupIdleUiState()
                    }
                    is MainViewModel.CurrencyListEvent.Loading -> {
                        binding.progress.isVisible = true
                        setupConnectingUiState()
                    }
                    else -> Unit
                }
            })

            viewModel.conversion.observe(this@MainActivity, Observer {
                when (it) {
                    is MainViewModel.CurrencyEvent.Success -> {
                        binding.progress.isVisible = false
                        binding.destEditTxt.setText(it.resultText)
                        setupIdleUiState()
                    }
                    is MainViewModel.CurrencyEvent.Failure -> {
                        binding.progress.isVisible = false
                        if (it.errorText.first) {
                            CommonUtils.showOneButtonDialog(this@MainActivity, it.errorText.second)
                        } else {
                            binding.srcInputErrorTxt.isVisible = true
                        }
                        setupIdleUiState()
                    }
                    is MainViewModel.CurrencyEvent.Loading -> {
                        binding.progress.isVisible = true
                        setupConnectingUiState()
                    }
                    else -> Unit
                }
            })
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (currentFocus != null) {
            val action = event.actionMasked
            val location = IntArray(2)
            binding.srcEditTxt.getLocationOnScreen(location)
            val mRect = Rect()
            mRect.left = location[0]
            mRect.top = location[1]
            mRect.right = location[0] + binding.srcEditTxt.width
            mRect.bottom = location[1] + binding.srcEditTxt.height
            val x = event.x.toInt()
            val y = event.y.toInt()
            if (action == MotionEvent.ACTION_DOWN && !mRect.contains(x, y)) {
                binding.srcEditTxt.clearFocus()
            }
        }
        return super.dispatchTouchEvent(event)
    }

    private fun setToolBar() {
        supportActionBar!!.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    this,
                    R.color.white
                )
            )
        )
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            supportActionBar!!.title = Html.fromHtml(
                String.format(
                    getString(R.string.toolbar_spanned_title), getString(
                        R.string.app_name
                    )
                ), Html.FROM_HTML_MODE_LEGACY
            )
        } else {
            supportActionBar!!.title = Html.fromHtml(
                String.format(
                    getString(R.string.toolbar_spanned_title), getString(
                        R.string.app_name
                    )
                )
            )
        }
    }

    private fun setListeners() {
        binding.btnConvert.setOnClickListener {
            setupConnectingUiState()
            val srcItem = binding.srcCurrencyDropDown.selectedItem as? CurrencyDetails
            val destItem = binding.destCurrencyDropDown.selectedItem as? CurrencyDetails
            viewModel.convert(
                this,
                binding.srcEditTxt.text?.toString() ?: Constants.EMPTY,
                srcItem?.code ?: Constants.EMPTY,
                destItem?.code ?: Constants.EMPTY
            )
        }

        binding.srcCurrencyDropDown.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (isSrcDropDownClicked) {
                        binding.srcEditTxt.setText(Constants.EMPTY)
                        binding.destEditTxt.setText(Constants.EMPTY)
                    }
                    isSrcDropDownClicked = false
                    viewModel.getSymbol(true, currencyAdapter.getItem(position))
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }

        binding.destCurrencyDropDown.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (isDestDropDownClicked) {
                        binding.destEditTxt.setText(Constants.EMPTY)
                    }
                    isDestDropDownClicked = false
                    viewModel.getSymbol(false, currencyAdapter.getItem(position))
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }

        binding.srcCurrencyDropDown.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    isSrcDropDownClicked = true
                }
                return false
            }
        })

        binding.destCurrencyDropDown.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_UP) {
                    isDestDropDownClicked = true
                }
                return false
            }
        })

        binding.swapBtn.setOnClickListener {
            binding.swapBtn.startAnimation(CommonUtils.rotateAnimation())
            isSrcDropDownClicked = false
            isDestDropDownClicked = false
            collectUiValues()
        }

        binding.srcEditTxt.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                CommonUtils.hideKeyboard(v)
            } else {
                binding.srcEditTxt.setSelection(binding.srcEditTxt.text?.length ?: 0)
            }
        }

        binding.srcEditTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                binding.destEditTxt.setText(Constants.EMPTY)
                binding.srcInputErrorTxt.isVisible = false
            }
        })

        binding.srcCurrencySymbol.setOnClickListener {
            binding.srcEditTxt.requestFocus()
            binding.srcEditTxt.setSelection(binding.srcEditTxt.text?.length ?: 0)
            CommonUtils.showKeyboard(it)
        }

    }

    private fun collectUiValues() {
        srcCurrencyPosition = binding.srcCurrencyDropDown.selectedItemPosition
        destCurrencyPosition = binding.destCurrencyDropDown.selectedItemPosition
        viewModel.handleSwap(
            binding.srcEditTxt.text?.toString() ?: Constants.EMPTY,
            binding.destEditTxt.text?.toString() ?: Constants.EMPTY
        )
    }

    private fun setupIdleUiState() {
        binding.btnConvert.isEnabled = true
        binding.swapBtn.isEnabled = true
        binding.srcEditTxt.isEnabled = true
        binding.srcCurrencyDropDown.isEnabled = true
        binding.destCurrencyDropDown.isEnabled = true
    }

    private fun setupConnectingUiState() {
        binding.btnConvert.isEnabled = false
        binding.swapBtn.isEnabled = false
        binding.srcEditTxt.isEnabled = false
        binding.srcCurrencyDropDown.isEnabled = false
        binding.destCurrencyDropDown.isEnabled = false
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.apply {
            srcCurrencyPosition = getInt(Constants.SOURCE_POSITION)
            destCurrencyPosition = getInt(Constants.DEST_POSITION)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.apply {
            putInt(Constants.SOURCE_POSITION, binding.srcCurrencyDropDown.selectedItemPosition)
            putInt(Constants.DEST_POSITION, binding.destCurrencyDropDown.selectedItemPosition)
        }
        super.onSaveInstanceState(outState)
    }

}